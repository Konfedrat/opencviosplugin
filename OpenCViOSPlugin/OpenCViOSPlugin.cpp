//
//  OpenCViOSPlugin.cpp
//  OpenCViOSPlugin
//
//  Created by Ihor Konfedrat on 31.03.2023.
//

#include "OpenCViOSPlugin.hpp"
#include "DebugCPP.hpp"

#include <opencv2/imgproc.hpp>
#include <opencv2/objdetect/face.hpp>

Mat* CVMat()
{
    return new Mat();
}

Mat* CVMatWithRowsAndCols(int rows, int cols, int type)
{
    return new Mat(rows, cols, type);
}

Mat* CVMatWithSize(double size_width, double size_height, int type)
{
    return new Mat(Size(size_width, size_height), type);
}

Mat* CVMatWithScalar(int rows, int cols, int type,
           double v0, double v1, double v2, double v3)
{
    return new Mat(rows, cols, type, Scalar(v0, v1, v2, v3));
}

void CVMatDelete(Mat* pMat)
{
    if (pMat)
    {
        delete pMat;
        pMat = nullptr;
    }
}

int CVMatCols(Mat* pMat)
{
    if (pMat)
        return pMat->cols;
    return -1;
}

int CVMatRows(Mat* pMat)
{
    if (pMat)
        return pMat->rows;
    return -1;
}

int CVMatType(Mat* pMat)
{
    if (pMat)
        return pMat->type();
    return 0;
}

size_t CVMatTotal(Mat* pMat)
{
    if (pMat)
        return pMat->total();
    return 0;
}

int CVMatDims(Mat* pMat)
{
    if (pMat)
        return pMat->dims;
    return 0;
}

void CVMatSize(Mat* pMat, double vals[])
{
    if (pMat)
    {
        Size size = pMat->size();
        vals[0] = size.width;
        vals[1] = size.height;
    }
}

int CVMatSizeByIndex(Mat* pMat, int index)
{
    if (index == 0 || index == 1)
        if (pMat)
            return pMat->size[index];
    return 0;
}

bool CVMatIsContinuous(Mat* pMat)
{
    if (pMat)
        return pMat->isContinuous();
    return false;
}

bool CVMatIsSubmatrix(Mat* pMat)
{
    if (pMat)
        return pMat->isSubmatrix();
    return false;
}

size_t CVMatDataAddr(Mat* pMat)
{
    if (pMat)
        return (size_t)pMat->data;
    return 0;
}

size_t CVMatElemSize(Mat* pMat)
{
    if (pMat)
        return pMat->elemSize();
    return 0;
}

void CVTextureToMat(uchar* data, Mat* pMat, bool flip, int flipCode)
{
    if (!data) return;
    if (!pMat) return;

    memcpy(pMat->data, data, pMat->total() * pMat->elemSize());

    if (flip)
    {
        cv::flip(*pMat, *pMat, flipCode);
    }
}

void CVFlip(Mat* pSrc, Mat* pDst, int flipCode)
{
    if (!pSrc) return;
    if (!pDst) return;

    flip(*pSrc, *pDst, flipCode);
}

void CVRotate(Mat* pSrc, Mat* pDst, int rotateCode)
{
    rotate(*pSrc, *pDst, rotateCode);
}

void CVResize(Mat* pSrc, Mat* pDst, double size_width, double size_height)
{
    resize(*pSrc, *pDst, Size(size_width, size_height));
}

void CVPutText(Mat* pMat,
               const char* text,
               double org_x,
               double org_y,
               int fontFace,
               double fontScale,
               double color_val0,
               double color_val1,
               double color_val2,
               double color_val3)
{
    putText(*pMat,
            text,
            Point(org_x, org_y),
            fontFace,
            fontScale,
            Scalar(color_val0, color_val1, color_val2, color_val3));
}

void CVPutTextWithStyleOptions(Mat* pMat,
                               const char* text,
                               double org_x,
                               double org_y,
                               int fontFace,
                               double fontScale,
                               double color_val0,
                               double color_val1,
                               double color_val2,
                               double color_val3,
                               int thickness,
                               int lineType,
                               bool bottomLeftOrigin)
{
    putText(*pMat,
            text,
            Point(org_x, org_y),
            fontFace,
            fontScale,
            Scalar(color_val0, color_val1, color_val2, color_val3),
            thickness,
            lineType,
            bottomLeftOrigin);
}

void CVTColor(Mat* pSrc, Mat* pDst, int code)
{
    cvtColor(*pSrc, *pDst, code);
}

void CVRectangle(Mat* pImg,
                 double pt1_x,
                 double pt1_y,
                 double pt2_x,
                 double pt2_y,
                 double color_val0,
                 double color_val1,
                 double color_val2,
                 double color_val3,
                 int thickness)
{
    if (pImg)
        rectangle(*pImg,
                  Point(pt1_x, pt1_y),
                  Point(pt2_x, pt2_y),
                  Scalar(color_val0, color_val1, color_val2, color_val3),
                  thickness);
}

void CVCircle(Mat* pImg,
              double center_x,
              double center_y,
              int radius,
              double color_val0,
              double color_val1,
              double color_val2,
              double color_val3,
              int thickness)
{
    if (pImg)
        circle(*pImg,
               Point(center_x, center_y),
               radius,
               Scalar(color_val0, color_val1, color_val2, color_val3),
               thickness);
}
void CVGetTextSize(const char* text,
                   int fontFace,
                   double fontScale,
                   int thickness,
                   int baseLine[],
                   double vals[])
{
    Size textSize = getTextSize(text, fontFace, fontScale, thickness, baseLine);
    vals[0] = textSize.width;
    vals[1] = textSize.height;
}

FaceDetector* CVFaceDetectorYNCreate(const char* model,
                                     const char* config,
                                     double input_size_width,
                                     double input_size_height,
                                     float score_threshold,
                                     float nms_threshold,
                                     int top_k)
{
    return new FaceDetector(model,
                            config,
                            input_size_width,
                            input_size_height,
                            score_threshold,
                            nms_threshold,
                            top_k);
}

int CVFaceDetectorYNDetect(FaceDetector* pDetector, Mat* pImage, Mat* pFaces)
{
    if (pDetector && pImage)
    {
        Ptr<FaceDetectorYN> detector = pDetector->getFaceDetectorYN();
        if (detector) {
            detector->setInputSize(pImage->size());
            return detector->detect(*pImage, *pFaces);
        }
    }

    return 0;
}

void CVFaceDetectorYNDelete(FaceDetector* pDetector)
{
    if (pDetector)
    {
        delete pDetector;
        pDetector = nullptr;
    }
}

int CVMatGetFace(Mat* pFaces, int row, int count, float vals[])
{
    if (pFaces)
    {
        for (int i = 0; i < 15; i++)
        {
            if (i < count)
                vals[i] = pFaces->at<float>(row, i);
        }
    }

    return count;
}
