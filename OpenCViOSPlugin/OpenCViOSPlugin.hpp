//
//  OpenCViOSPlugin.hpp
//  OpenCViOSPlugin
//
//  Created by Ihor Konfedrat on 31.03.2023.
//

#ifndef OpenCViOSPlugin_hpp
#define OpenCViOSPlugin_hpp

#include <opencv2/core/mat.hpp>
#include "FaceDetector.hpp"

using namespace cv;

extern "C" {
    Mat* CVMat();
    Mat* CVMatWithRowsAndCols(int rows, int cols, int type);
    Mat* CVMatWithSize(double size_width, double size_height, int type);
    Mat* CVMatWithScalar(int rows, int cols, int type, double v0,
                         double v1, double v2, double v3);
    void CVMatDelete(Mat* pMat);

    int CVMatCols(Mat* pMat);
    int CVMatRows(Mat* pMat);
    int CVMatType(Mat* pMat);
    size_t CVMatTotal(Mat* pMat);
    int CVMatDims(Mat* pMat);
    void CVMatSize(Mat* pMat, double vals[]);
    int CVMatSizeByIndex(Mat* pMat, int index);
    bool CVMatIsContinuous(Mat* pMat);
    bool CVMatIsSubmatrix(Mat* pMat);
    size_t CVMatDataAddr(Mat* pMat);

    size_t CVMatElemSize(Mat* pMat);

    void CVTextureToMat(uchar* data, Mat* pMat, bool flip, int flipCode);
    void CVFlip(Mat* pSrc, Mat* pDst, int flipCode);
    void CVRotate(Mat* pSrc, Mat* pDst, int rotateCode);
    void CVResize(Mat* pSrc, Mat* pDst, double size_width, double size_height);
    void CVPutText(Mat* pMat,
                   const char* text,
                   double org_x,
                   double org_y,
                   int fontFace,
                   double fontScale,
                   double color_val0,
                   double color_val1,
                   double color_val2,
                   double color_val3);
    void CVPutTextWithStyleOptions(Mat* pMat,
                                   const char* text,
                                   double org_x,
                                   double org_y,
                                   int fontFace,
                                   double fontScale,
                                   double color_val0,
                                   double color_val1,
                                   double color_val2,
                                   double color_val3,
                                   int thickness,
                                   int lineType,
                                   bool bottomLeftOrigin);
    void CVTColor(Mat* pSrc, Mat* pDst, int code);
    void CVRectangle(Mat* pImg,
                     double pt1_x,
                     double pt1_y,
                     double pt2_x,
                     double pt2_y,
                     double color_val0,
                     double color_val1,
                     double color_val2,
                     double color_val3,
                     int thickness);
    void CVCircle(Mat* pImg,
                  double center_x,
                  double center_y,
                  int radius,
                  double color_val0,
                  double color_val1,
                  double color_val2,
                  double color_val3,
                  int thickness);
    void CVGetTextSize(const char* text,
                       int fontFace,
                       double fontScale,
                       int thickness,
                       int baseLine[],
                       double vals[]);

    FaceDetector* CVFaceDetectorYNCreate(const char* model,
                                           const char* config,
                                           double input_size_width,
                                           double input_size_height,
                                           float score_threshold,
                                           float nms_threshold,
                                           int top_k);

    int CVFaceDetectorYNDetect(FaceDetector* pDetector, Mat* pImage, Mat* pFaces);
    void CVFaceDetectorYNDelete(FaceDetector* pDetector);

    int CVMatGetFace(Mat* pFaces, int row, int count, float vals[]);
}

#endif /* OpenCViOSPlugin_hpp */
